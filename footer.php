        <footer class="site-footer outer">
            <div class="site-footer-content inner">
                <nav class="site-footer-nav">
                    <?php wp_nav_menu(); ?>
                </nav>
                theme by <a href="https://rowan.website">Rowan Lupton</a>
            </div>
        </footer>
        <?php wp_footer(); ?>
    </body>
</html>