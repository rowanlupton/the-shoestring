<?php

function initialize() {
    add_theme_support( 'automatic-feed-links' );

    add_theme_support( 'title-tag' );

    add_theme_support( 'post-thumbnails' );
    add_image_size( 'shoestring-featured-image', 
2000, 1200, true);
    add_image_size( 'shoestring-thumbnail-avatar', 
100, 100, true);

    $GLOBALS['content_width'] = 700;

    register_nav_menus( array(
        'nav'       => __('Navigation', 'shoestring' ),
        'nav-cta'  => __('Nav Menu CTAs', 'shoestring' ),
        'social'    => __('Social Links', 'shoestring'),
    ));

    add_theme_support( 'html5', array(
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ));

    add_theme_support( 'post-formats', array(
        'aside',
        'image',
        'video',
        'link',
        'gallery',
        'audio',
    ));

    add_theme_support( 'custom-logo', array(
        'flex-width'    => true,
        'flex-height'   => true,
        'header-text'   => array(
            'site-title',
            'site-description' ),
    ));
}
add_action( 'after_setup_theme', 'initialize' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function initialize_widgets() {
    register_sidebar( array(
        'name'          => __( 'Blog Upper Sidebar', 'shoe' ),
        'id'            => 'sidebar-upper',
        'description'   => __( 'Add widgets here to appear in your sidebar', 'shoe' ),
        'before_widget' => '<section id="sidebar-upper" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Blog Lower Sidebar', 'shoe' ),
        'id'            => 'sidebar-lower',
        'description'   => __( 'Add a custom HTML widget to embed a scrollable social feed.', 'shoe' ),
        'before_widget' => '<section id="sidebar-lower" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Footer 1', 'twentyseventeen' ),
        'id'            => 'sidebar-2',
        'description'   => __( 'Add widgets here to appear in your footer.', 'twentyseventeen' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'initialize_widgets' );

function recent_posts() {
    $recents = new WP_Query( 'posts_per_page=3' ); ?>

    <?php while ( $recents->have_posts() ) : $recents-> the_post(); ?>
        <?php get_template_part( 'partials/sidebar-card', get_post_format() ); ?>
    <?php endwhile;
}

function shoestring_scripts() {
    wp_enqueue_style( 'normalize.css', get_stylesheet_directory_uri()."/node_modules/normalize.css/normalize.css" );
    wp_enqueue_style( 'font-awesome', 'https://use.fontawesome.com/releases/v5.1.1/css/all.css' );
    // wp_enqueue_style( 'fonts', get_stylesheet_directory_uri()."/assets/fonts/fonts.css");
    wp_enqueue_style( 'fonts', get_stylesheet_directory_uri()."/assets/fonts/league-gothic/webfonts/stylesheet.css");

    wp_enqueue_style( 'shoestring-style', get_stylesheet_uri() ); // theme styles
    // require get_template_directory() . '/inc/customizer.php';
}
add_action( 'wp_enqueue_scripts', 'shoestring_scripts' );

?>