<!DOCTYPE html>
<html lang="en">
    <head>
        <?php wp_head();?>
    </head>

    <body>
        <header class="site-header">
            <div class="inner">
                <a href="/" class="site-title">
                    <?php
                    if (has_custom_logo()) :
                        the_custom_logo();
                    else :
                        echo get_bloginfo( 'name' );
                    endif; ?>
                </a>
                <nav class="site-header-nav">
                    <?php wp_nav_menu(array('theme_location' => 'nav')); ?>
                </nav>

                <?php wp_nav_menu(array('theme_location' => 'nav-cta')); ?>
                <?php wp_nav_menu(array('theme_location' => 'social')); ?>
            </div>
        </header>