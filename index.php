<?php get_header(); ?>

<main id="main" class="main outer">
    <div class="inner">
        <section id="posts">
            <?php 
                if ( have_posts() ) : while ( have_posts() ) : the_post();
        
                    get_template_part( 'partials/post-card', get_post_format() );
      
                endwhile; endif; 
            ?>
        </section>
    </div>
</main>
<?php get_sidebar(); ?>

<?php get_footer(); ?>