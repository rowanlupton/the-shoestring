<div class="post-single">
    <img src="<?php the_post_thumbnail_url(); ?>" />
    <a href="<?php the_permalink(); ?>" class="inner">
        <div class="content">
            <h1><?php the_title(); ?></h1>
            <p><?php the_content(); ?></p>
        </div>
    </a>
</div>