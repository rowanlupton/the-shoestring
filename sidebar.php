<section id="sidebar">
    <?php if ( is_active_sidebar( 'sidebar-upper' ) ) :
        dynamic_sidebar( 'sidebar-upper' );
    else : ?> 
        <section id="sidebar-upper"> 
            <h1>Footprints</h1>
            <?php echo recent_posts();?>     
        </section>
    <?php endif; ?>

    <?php if ( is_active_sidebar( 'sidebar-lower' ) ) : ?>
        <?php dynamic_sidebar( 'sidebar-lower' ); ?>
    <?php endif; ?>
</section>